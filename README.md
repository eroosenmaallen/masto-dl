# Masto-DL

Masto-DL is a script to archive your Mastodon history, toot-by-toot.


## Present State

This script is in a very alpha state; it has been tested to not catch fire,
but it may eat your kitten and/or /usr/bin. Use with care.


## Basic Usage

`masto-dl init <directory>` will interactively configure the specified
directory, saving the configuration as JSON in `masto-dl.conf`. If the
directory is not specified, it will default to the current dir.

`masto-dl run <directory>` will begin downloading all your toots. Toots are
saved under the specified directory below a directory structure of
`year/month/toot-id.json`, eg. `2022/11/109385308583886341.json`.

*(NYI)* `masto-dl start <directory>` will start a live bot which will archive
your toots as they happen.


## Detailed Usage

* create an Access Token: in the web UI, navigate to Settings > Development and create a *New Applicaition*
* Give it a name, made sure the *read* scope is checked (you can remove the other scopes if you want)
* After you submit that, click on your new application in the list and copy the code from beside *Your Access Token*.
* Back to the commandline, run `npx masto-dl init` and it will ask for your instance url and API token.
* Finally, `npx masto-dl run` will start downloading. You can add `--verbose` to get more feedback about what it's doing, or just let it go

Your toots will be archived to json files under a directory structure for the year, months, and toot ID, like `yyyy/mm/12345.json` - support for yaml is coming soon


## Roadmap

* be smarter about where to look; cache and reuse the `oldest_id` and
  `newest_id` we've seen, so we can start looking in a reasonable place for
  newer and older toots
* more output formats - store toots as text or YAML


