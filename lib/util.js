/**
 *  @file       lib/util.js
 *  @author     Eddie Roosenmaallen <silvermoon82@gmail.com>
 *  @date       November 2022
 *
 *  This module provides some uitility functions
 */

'use strict';

const fs = require('fs');
const path = require('path');
const yaml = require('yaml');

const exitCodes = {
  EXIT_OK:            0,  // no error
  EXIT_ERROR:         1,  // Error, not otherwise specified
  EXIT_ERROR_CONFIG:  2,  // Failed to load local config
};

// Default config object; also defines permissible properties
const defaultConfig = {
  _path: null,
  api_domain: null,
  api_token: null,
  my_id: null,
};

// Default cache object; also defined permissible properties
const defaultCache = {
  _path: null,
  prev: null,
  next: null,
  oldest_id: null,
  newest_id: null,
}


/**
 *  Copy nonnull enumerable own properties from `sources` onto `dest`
 *
 *  @param  {Object}    dest    Destination object. Will be mutated.
 *  @param  {...Object} sources Source objects. Properties will be assigned
 *    left-first, so in the case of duplicate property names, the
 *    last-listed value will win
 *
 *  @return {Object}            dest, with new properties
 */
function objectAssignNonnull(dest, ...sources)
{
  sources.forEach(src => {
    for (let [p, v] of Object.entries(src))
    {
      if (typeof v === 'undefined' || v === null)
        continue;
      dest[p] = v;
    }
  });

  return dest;
}


/**
 *  Copy nonnull enumberable own properties _which already exist on dest_ from
 *  one or more source objects.
 *
 *  @param  {Obect}    dest     Object to update. Will be mutated in place as well as returned.
 *  @param  {...Object} sources One or more source objects to copy values from
 *  @return {Object}            Original dest object, with property values updated
 */
function objectAssignExisting(dest, ...sources)
{
  sources.forEach(src => {
    for (let [p, v] of Object.entries(src))
    {
      if (typeof v === 'undefined' || v === null || !dest.hasOwnProperty(p))
        continue;
      dest[p] = v;
    }
  });

  return dest;
}


/**
 *  Copy nonnull enumerable own properties from `sources` onto `dest`,
 *  without clobbering existing properties.
 *
 *  @param  {Object}    dest    Destination object. Will be mutated.
 *  @param  {...Object} sources Source objects. Properties will be assigned
 *    left-first, so in the case of duplicate property names, the
 *    first-listed value will win
 *
 *  @return {Object}            dest, with new properties
 */
function objectAssignNoclobber(dest, ...sources)
{
  sources.forEach(src => {
    for (let [p, v] of Object.entries(src))
    {
      if (typeof v === 'undefined' || v === null)
        continue;
      if (typeof dest[p] !== 'undefined')
        continue;
      dest[p] = v;
    }
  });

  return dest;
}


/**
 *  Handle a fatal error processing a command
 *
 *  @param  {Error} error Error caught by cli framework
 */
function handleCommandError(msg, error, yargs)
{
  const code = '' + (error.code || null);
  console.log({msg, error});

  if (code.startsWith('ESYNTAX.'))
  {
    console.error(`Fatal error: ${error.message}
Filename: ${error.fileName}
Position: ${error.column}`);
    process.exit(EXIT_ERROR_CONFIG);
  }

  console.error(`Unexpected ${error.name}:`, error.message);
  console.error(error.stack);
  process.exit(EXIT_ERROR);
}


/**
 *  Load a specified configuration file
 *
 *  @param  {String} filePath Path to the data file to open
 *  @return {Object}          Configuration parsed from the file
 */
function loadDataFile(filePath)
{
  filePath = path.resolve(filePath);
  const data = {
    '_path': filePath,
    '_format': null,
  };

  // If local data doesn't exist, return empty object
  if (!fs.existsSync(filePath))
    return data;

  try
  {
    const fileContent = fs.readFileSync(filePath, 'utf8');
    data._format = (fileContent.charAt(0) === '{')
      ? 'json'
      : 'yaml';
    const fileData = (data._format === 'json')
      ? JSON.parse(fileContent)
      : YAML.parse(fileContent);

    objectAssignNonnull(data, fileData);
    data._path = filePath;
  }
  catch (error)
  {
    const msg = '' + error.message;

    error.message = 'Error loading data: ' + msg;
    error.fileName = filePath;

    if (error instanceof SyntaxError)
    {
      const [, column] = msg.match(/on line ([0-9]+)$/);

      if (column)
        error.column = column;

      error.code = 'ESYNTAX.DATA.' + data._format.toUpperCase();
    }

    throw error;
  }

  return data;
}


/**
 *  Load a local configuration, given a parsed options object
 *
 *  @param  {Object|String} argv  Parsed CLI options from yargs, or string path
 *  @return {Object}              Configuration from loadDataFile
 */
function loadConfig(argv)
{
  const configFile = getConfigPath(argv);
  const configData = loadDataFile(configFile);
  const config = objectAssignExisting(
    Object.assign({}, defaultConfig),
    configData
  );

  debugging('config') && console.debug(`208: loaded config file:`, { configFile, configData, config });

  return config;
}


/**
 *  Load a local cache, given a parsed options object
 *
 *  @param  {Object|String} argv  Parsed CLI options from yargs, or string path
 *  @return {Object}              Cache object from loadDataFile
 */
function loadCache(argv)
{
  const cacheFile = getConfigPath(argv, 'masto-dl.cache');
  const cacheData = loadDataFile(cacheFile);
  const cache = objectAssignExisting(
    Object.assign({}, defaultCache),
    cacheData
  );

  debugging('cache') && console.debug(`229: loaded cache file:`, {cacheFile, cacheData, cache});

  return cache;
}


/**
 *  Determine path to config file, given an argv object containing directory
 *    and configFile properties
 *
 *  @param  {Object|String} argv             Arguments parsed from
 *                                           commandline, or string path
 *  @param  {Object?}       argv.directory   Working directory
 *  @param  {Object?}       argv.configFile  Name of config file
 *  @param  {String?}       filename         Filename to save under
 *  @return {String}                         Absolute path to the config file
 */
function getConfigPath(argv, filename = '')
{
  const basePath = (typeof argv === 'string')
    ? path.resolve(argv)
    : path.resolve(argv.directory);

  if (!fs.existsSync(basePath))
  {
    console.error(`Error: Configuration path "${basePath}" does not exist`);
    process.exit(exitCodes.EXIT_CONFIG_NOTDIR);
  }

  return path.resolve(basePath, filename || argv.configFile);
}


/**
 *  Save a config file at a specified path
 *
 *  @param  {Object}        config    Configuration options (will not be mutated in place)
 *  @param  {String|Object} filePath  Path to save to, or argv object to determine it from
 *  @return {Object}                  config object as written, with path property updated
 */
function saveConfigFile(config, filePath)
{
  if (!filePath)
    filePath = getConfigPath(config._path || './masto-dl.conf');
  else
    filePath = getConfigPath(filePath);

  const configToWrite = objectAssignExisting(Object.assign({}, defaultConfig), config);
  delete configToWrite._path,

  fs.writeFileSync(filePath, JSON.stringify(configToWrite, null, 2));

  configToWrite._path = filePath;

  return configToWrite;
}

/**
 *  Save a cache file at a specified path
 *
 *  @param  {Object}        cache    Configuration options (will not be mutated in place)
 *  @param  {String|Object} filePath  Path to save to, or argv object to determine it from
 *  @return {Object}                  cache object as written, with path property updated
 */
function saveCacheFile(cache, filePath)
{
  if (!filePath)
    filePath = getConfigPath(cache._path || './', 'masto-dl.cache');
  else
    filePath = getConfigPath(filePath, 'masto-dl.cache');

  const cacheToWrite = objectAssignExisting(Object.assign({}, defaultCache), cache);
  delete cacheToWrite._path;
  delete cacheToWrite._format;

  fs.writeFileSync(filePath, JSON.stringify(cacheToWrite, null, 2));

  cacheToWrite._path = filePath;
  cacheToWrite._format = 'json';

  return cacheToWrite;
}


/**
 *  Check whether $DEBUG env var is active and matches an optional tag
 *
 *  @param  {string} tag Optional tag to match against tokens in $DEBUG
 *  @return {boolean}     True if debugging is active and tag is unspecified
 *    or is present in environment
 */
function debugging(tag)
{
  // if $DEBUG isn't set, bail early with 'no'
  if (!process.env.DEBUG)
    return false;

  // if tag isn't set, bail early with 'yes'
  if (!tag)
    return true;

  // Extract individual tokens from $DEBUG
  const tokens = process.env.DEBUG.split(/\s*,\s*/).map(t => t.toLowerCase());

  // If $DEBUG is promiscuous, then print the ddebug
  if (['*', 'all', 'true', '1'].some(t => tokens.includes(t)))
    return true;

  // If tag is included in $DEBUG tokens, then print the debug
  if (tokens.includes(tag.toLowerCase()))
    return true;

  return false;
}


Object.assign(exports, {
  objectAssignNonnull,
  objectAssignExisting,
  objectAssignNoclobber,
  loadConfig,
  loadCache,
  loadDataFile,
  saveConfigFile,
  saveCacheFile,
  getConfigPath,
  debugging,
  handleCommandError,
  ...exitCodes,
});
