#! /usr/bin/env node
/**
 *  @file       index.js
 *  @author     Eddie Roosenmaallen <silvermoon82@gmail.com>
 *  @date       November 2022
 *
 *  Masto-DL
 *
 *  Download and archive your Mastodon feed locally
 */

'use strict';

const yargs = require('yargs')(process.argv.slice(2));

const { handleCommandError } = require('./lib/util');

yargs
  .options({
    domain: {
      describe: 'Instance\'s domain name',
      type: 'string',
    },
    apiToken: {
      describe: 'API token (from https://your-instance/settings/applications)',
      type: 'string',
    },

    configFile: {
      describe: 'Name of the local config file',
      default: 'masto-dl.conf',
      type: 'path',
    },
  })
  .commandDir('./commands')
  .recommendCommands()
  .demandCommand()
  .strict()
  // .fail(handleCommandError)
  .argv;
