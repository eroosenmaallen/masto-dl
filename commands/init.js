/**
 *  @file       commands/init.js
 *  @author     Eddie Roosenmaallen <silvermoon82@gmail.com>
 *  @date       November 2022
 *
 *  The init command creates a new Masto-DL configuration
 *  
 */

'use strict';

const fs = require('fs');
const path = require('path');
const readline = require('readline');

const {
  loadConfig,
  saveConfigFile,
} = require('../lib/util');

var _rl = false;



exports.command = 'init [directory]';

exports.describe = 'Create a new Masto-DL configuration';

exports.builder = {
  'directory': {
    describe: 'The directory in which to store the configuration and data',
    default: '.',
    type: 'path',
  },
};


exports.handler = async function init$$handler(argv) {
  const config = loadConfig(argv, false);

  console.debug(`
Config file:      ${config._path}
Configuration:    ${JSON.stringify(config, null, 2)}`);

  const domain = (argv.domain || await rl_question(
    'Please enter your instance\'s domain name: ',
    config.api_domain,
  )).replace(/^(.*?\/)?([^/]+)(\/.*)?$/, '$2');
  const api_url = new URL('https://' + domain).href;

  const api_token = argv.api_token || await rl_question(
    `Please enter your API token from https://${domain}/settings/applications: `,
    config.api_token,
  );

  rl_close();

  console.debug(`
API URL:   ${api_url}
API Token: ${api_token}`);

  const mb = new (require('mastobot').MastoBot)(api_url, {
    api_token,
  });

  let res = await mb.checkCredentials();
  const me = res.data;

  console.debug(`
Credentials check:`, me.display_name, `@${me.acct}@${domain}`);

  config.api_domain = domain;
  config.api_token = api_token;
  config.my_id = me.id;

  if (!argv.dryRun)
  {
    const { _path } = saveConfigFile(config, argv);
    console.log('Saved configuration to:', config._path);
  }
};




function rl_question(prompt = '?', defaultAnswer = '')
{
  const rl = get_rl();

  return new Promise((resolve, reject) => {
    const onError = error => {
      rl.off('error', onError);
      reject(error);
    }
    rl.on('error', onError);

    rl.question(prompt, answer => {
      rl.off('error', onError);
      resolve('' + (answer || defaultAnswer));
    });
  });
}

function get_rl()
{
  if (_rl)
    return _rl;

  return _rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
}

function rl_close()
{
  _rl.close();
  _rl = false;
}

