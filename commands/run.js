/**
 *  @file       commands/run.js
 *  @author     Eddie Roosenmaallen <silvermoon82@gmail.com>
 *  @date       November 2022
 *
 *  Fetch and archive the authenticated user's stream
 */

'use strict';

const fs = require('fs');
const path = require('path');
const readline = require('readline');
const { MastoBot } = require('mastobot');

const {
  loadConfig,
  saveConfigFile,
  loadCache,
  saveCacheFile,
  debugging,
} = require('../lib/util');


exports.command = 'run [directory]';

exports.describe = 'Download and save any toots older or newer than already saved';

exports.builder = {
  'directory': {
    describe: 'The directory in which to store the configuration and data',
    default: '.',
    type: 'path',
  },
  'verbose': {
    describe: 'Be more chatty about what\'s being done',
    default: false,
    type: 'boolean',
  },
  'quiet': {
    describe: 'Be less chatty about what\'s happening',
    default: false,
    type: 'boolean',
  },
  'refetch': {
    describe: 'If set, will ignore toots it has already downloaded, and will fetch the user\'s entire history',
  default: false,
    type: 'boolean',
  },
};


exports.handler = async function run$$handler(argv) {
  const config = loadConfig(argv);
  const cache = loadCache(argv);

  debugging('config') && console.log(`057: loaded config & cache`, { config, cache });
  const directory = path.dirname(config._path);

  const { api_domain, api_token } = config;
  const api_url = new URL('https://' + config.api_domain).href;

  const mb = new MastoBot(api_url, { api_token });
  const me = (await mb.checkCredentials()).data;

  console.info(`Connected as @${me.acct}@${api_domain}`);

  let { newest_id, oldest_id, next, prev } = cache;

  let stop = false;
  let downloaded = 0, saved = 0;


  // If we have a "prev" link (from our newest Toot), follow that way first 
  if (cache.prev)
  {
    argv.quiet || console.log('Checking for newer toots...');
    const { prev, next } = await fetchToots(await mb.get(cache.prev), 'prev');
    cache.prev = prev || cache.prev;
  }

  if (cache.next && !argv.refetch)
  {
    argv.quiet || console.log('Looking for older toots...');
    const { next } = await fetchToots(await mb.get(cache.next));
    cache.next = next || cache.next;
  }
  else
  {
    argv.quiet || console.log('Fetching all your toots...')
    const links = await fetchToots(await mb.getAccountStatuses(me.id));
    debugging('cache') && console.log(`092: got links`, links);
    Object.assign(cache, links);
  }

  // const savedConfig = saveConfigFile(config, argv);
  // debugging('config') && console.log('Saved config:', savedConfig);

  debugging('cache') && console.log('Saving cache:', cache);
  const savedCache = saveCacheFile(cache, argv);
  debugging('cache') && console.log('Saved cache:', savedCache);
  
  console.log(`Downloaded ${downloaded} toots in total, saved ${saved} new`);

  async function fetchToots(page, direction = 'next')
  {
    function stopFetching() {
      process.off('SIGINT', stopFetching);
      process.off('SIGQUIT', stopFetching);
      console.log('Stopping...');
      stop = true;
    }

    process.once('SIGINT', stopFetching);
    process.once('SIGQUIT', stopFetching);

    const links = {
      prev: cache.prev || null,
      next: cache.next || null,
    };

    do {
      const newest = page.data[0];
      const [oldest] = page.data.slice(-1);

      argv.quiet || console.log(`... got ${page.data.length} toots`, oldest ? `oldest = ${oldest?.created_at.toLocaleString()}` : '', newest ? `newest = ${newest?.created_at.toLocaleString()}` : '');

      const toots = page.data;
      downloaded += toots.length;

      let savedNew = 0;

      toots.forEach(toot => {
        const d = new Date(toot.created_at);
        const subdir = path.join('' + d.getFullYear(), (d.getMonth() + 1).toString(10).padStart(2, '0'));
        const tootDir = path.resolve(directory, subdir);
        const tootFileName = toot.id + '.json';
        const tootFile = path.resolve(tootDir, tootFileName);

        if (toot.account.id === me.id)
          toot.account = me.url;

        if (!links.prev)
          links.prev = page.link.prev;

        if (!cache.newest_id || toot.id > cache.newest_id)
        {
          cache.newest_id = toot.id;
          links.prev = page.link?.prev || links.prev;
        }

        if (!links.next)
          links.next = page.link.next;

        if (!cache.oldest_id || toot.id < cache.oldest_id)
        {
          cache.oldest_id = toot.id;
          links.next = page.link?.next || links.next;
        }

        if (!fs.existsSync(tootDir))
          fs.mkdirSync(tootDir, { recursive: true });

        if (!fs.existsSync(tootFile))
        {
          fs.writeFileSync(tootFile, JSON.stringify(toot, null, 2));
          debugging('saves') && console.debug(`  - wrote ${path.join(subdir, tootFileName)}`);
          saved++;
          savedNew++;
        }
        else
        {
          debugging('saves') && console.debug(`  - skip ${path.join(subdir, tootFileName)}; exists`);
        }
      });

      if (savedNew && argv.verbose)
        console.info(`    saved ${savedNew} new toots`);

      // Throttle our requests some so we don't anger instance admins
      await new Promise(resolve => setTimeout(resolve, 500));
    }
    while (
      !stop &&
      page.data.length &&
      page.link[direction] &&
      (page = await mb.get(page.link[direction])) &&
      page.data.length
    );

    process.off('SIGINT', stopFetching);
    process.off('SIGQUIT', stopFetching);

    return links;
  }
};

